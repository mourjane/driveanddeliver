package com.example.driveanddeliver.repository;


import com.example.driveanddeliver.domain.DeliveryMethod;
import com.example.driveanddeliver.domain.TimeSlot;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

import java.time.LocalDate;

public interface TimeSlotRepository extends ReactiveCrudRepository<TimeSlot, Long> {
    Flux<TimeSlot> findByDeliveryMethodAndDeliveryDateAndBookedFalse(DeliveryMethod deliveryMethod, LocalDate date);
}
