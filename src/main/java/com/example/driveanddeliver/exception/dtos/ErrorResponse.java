package com.example.driveanddeliver.exception.dtos;

import org.springframework.http.HttpStatus;

public record ErrorResponse(HttpStatus status, String message) {}
