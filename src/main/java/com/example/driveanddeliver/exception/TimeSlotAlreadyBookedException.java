package com.example.driveanddeliver.exception;

public class TimeSlotAlreadyBookedException extends RuntimeException {

    // Constructor with message parameter
    public TimeSlotAlreadyBookedException(String message) {
        super(message);
    }
}
