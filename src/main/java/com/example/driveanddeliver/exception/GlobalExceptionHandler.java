package com.example.driveanddeliver.exception;

import com.example.driveanddeliver.exception.dtos.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(TimeSlotAlreadyBookedException.class)
    public ResponseEntity<ErrorResponse> handleTimeSlotAlreadyBookedException(TimeSlotAlreadyBookedException ex) {
        log.error("Time slot is already booked", ex);
        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.BAD_REQUEST, "Time slot is already booked.");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(errorResponse);
    }
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handleException(Exception ex) {
        log.error("An unexpected error occurred", ex);
        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, "An error occurred. Please try again later.");

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(errorResponse);
    }
}