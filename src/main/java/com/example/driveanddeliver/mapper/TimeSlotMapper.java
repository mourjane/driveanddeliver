package com.example.driveanddeliver.mapper;

import com.example.driveanddeliver.controller.dtos.TimeSlotDTO;
import com.example.driveanddeliver.domain.TimeSlot;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TimeSlotMapper {
    TimeSlotDTO toTimeSlotDTO(TimeSlot timeSlot);
}
