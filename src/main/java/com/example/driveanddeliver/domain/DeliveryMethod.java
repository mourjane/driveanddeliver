package com.example.driveanddeliver.domain;

public enum DeliveryMethod {
    DRIVE,
    DELIVERY,
    DELIVERY_TODAY,
    DELIVERY_ASAP
}
