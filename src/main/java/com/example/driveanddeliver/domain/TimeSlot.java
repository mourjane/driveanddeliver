package com.example.driveanddeliver.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table("time_slots")
public class TimeSlot {
    @Id
    private Long id;
    private DeliveryMethod deliveryMethod;
    private LocalDate deliveryDate;
    private LocalTime startTime;
    private LocalTime endTime;
    private boolean booked;
}
