package com.example.driveanddeliver.service.impl;


import com.example.driveanddeliver.controller.dtos.TimeSlotDTO;
import com.example.driveanddeliver.domain.DeliveryMethod;
import com.example.driveanddeliver.exception.TimeSlotAlreadyBookedException;
import com.example.driveanddeliver.mapper.TimeSlotMapper;
import com.example.driveanddeliver.repository.TimeSlotRepository;
import com.example.driveanddeliver.service.DeliveryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class DeliveryServiceImpl implements DeliveryService {

    private final TimeSlotRepository timeSlotRepository;
    private final TimeSlotMapper timeSlotMapper;

    // Method to find available time slots based on delivery method and date
    @Override
    public Flux<TimeSlotDTO> findAvailableTimeSlots(DeliveryMethod method, LocalDate date) {
        return timeSlotRepository.findByDeliveryMethodAndDeliveryDateAndBookedFalse(method, date).map(timeSlotMapper::toTimeSlotDTO);
    }

    // Method to book a time slot by its ID
    @Override
    public Mono<TimeSlotDTO> bookTimeSlot(Long id) {

        return timeSlotRepository.findById(id) // Find time slot by ID
                .flatMap(timeSlot -> {
                    if (!timeSlot.isBooked()) { // Check if the time slot is not already booked
                        timeSlot.setBooked(true); // Mark the time slot as booked
                        return timeSlotRepository.save(timeSlot).map(timeSlotMapper::toTimeSlotDTO); // Save the updated time slot to the repository
                    } else {
                        return Mono.error(new TimeSlotAlreadyBookedException("Time slot is already booked")); // Throw exception if the time slot is already booked
                    }
                });
    }
}
