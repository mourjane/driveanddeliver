package com.example.driveanddeliver.service;


import com.example.driveanddeliver.controller.dtos.TimeSlotDTO;
import com.example.driveanddeliver.domain.DeliveryMethod;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

public interface DeliveryService {

    Flux<TimeSlotDTO> findAvailableTimeSlots(DeliveryMethod method, LocalDate date);

    Mono<TimeSlotDTO> bookTimeSlot(Long id);

}
