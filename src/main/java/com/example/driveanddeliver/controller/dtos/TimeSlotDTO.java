package com.example.driveanddeliver.controller.dtos;


import com.example.driveanddeliver.domain.DeliveryMethod;

import java.time.LocalDate;
import java.time.LocalTime;

public record TimeSlotDTO(
        Long id,
        DeliveryMethod deliveryMethod,
        LocalDate deliveryDate,
        LocalTime startTime,
        LocalTime endTime,
        boolean booked
) {
}
