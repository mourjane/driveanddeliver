package com.example.driveanddeliver.controller;

import com.example.driveanddeliver.controller.dtos.TimeSlotDTO;
import com.example.driveanddeliver.domain.DeliveryMethod;
import com.example.driveanddeliver.service.DeliveryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@RestController
@RequestMapping("/api/delivery")
@RequiredArgsConstructor
public class DeliveryController {
    private final DeliveryService deliveryService;

    @GetMapping("/available-slots")
    public Flux<TimeSlotDTO> getAvailableTimeSlots(@RequestParam DeliveryMethod method, @RequestParam LocalDate date) {
        return deliveryService.findAvailableTimeSlots(method, date);
    }

    @PostMapping("/book-slot/{id}")
    public Mono<ResponseEntity<TimeSlotDTO>> bookTimeSlot(@PathVariable Long id) {
        return deliveryService.bookTimeSlot(id)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }
}