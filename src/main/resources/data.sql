CREATE TABLE IF NOT EXISTS time_slots (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    delivery_method VARCHAR(255) NOT NULL,
    delivery_date DATE NOT NULL,
    start_time TIME NOT NULL,
    end_time TIME NOT NULL,
    booked BOOLEAN NOT NULL
    );

-- Insert initial data into time_slots table
INSERT INTO time_slots (delivery_method, delivery_date, start_time, end_time, booked) VALUES
                                                                                ('DRIVE', '2024-06-01', '09:00:00', '12:00:00', false),
                                                                                ('DRIVE', '2024-06-01', '14:00:00', '17:00:00', false),
                                                                                ('DELIVERY', '2024-06-02', '10:00:00', '13:00:00', false),
                                                                                ('DELIVERY', '2024-06-02', '15:00:00', '18:00:00', false),
                                                                                ('DELIVERY_TODAY', '2024-06-03', '11:00:00', '14:00:00', false),
                                                                                ('DELIVERY_TODAY', '2024-06-03', '16:00:00', '18:00:00', false),
                                                                                ('DELIVERY_ASAP', '2024-06-04', '12:00:00', '15:00:00', false),
                                                                                ('DELIVERY_ASAP', '2024-06-04', '16:00:00', '19:00:00', false);