package com.example.driveanddeliver.service;

import com.example.driveanddeliver.controller.dtos.TimeSlotDTO;
import com.example.driveanddeliver.domain.DeliveryMethod;
import com.example.driveanddeliver.domain.TimeSlot;
import com.example.driveanddeliver.exception.TimeSlotAlreadyBookedException;
import com.example.driveanddeliver.mapper.TimeSlotMapper;
import com.example.driveanddeliver.repository.TimeSlotRepository;
import com.example.driveanddeliver.service.impl.DeliveryServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

public class DeliveryServiceTest {

    @Mock
    private TimeSlotRepository timeSlotRepository;

    @Mock
    private TimeSlotMapper timeSlotMapper;

    @InjectMocks
    private DeliveryServiceImpl deliveryService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindAvailableTimeSlots() {
        LocalDate date = LocalDate.now();
        when(timeSlotRepository.findByDeliveryMethodAndDeliveryDateAndBookedFalse(any(), any()))
                .thenReturn(Flux.empty());

        deliveryService.findAvailableTimeSlots(DeliveryMethod.DRIVE, date);

        verify(timeSlotRepository, times(1)).findByDeliveryMethodAndDeliveryDateAndBookedFalse(any(), any());

    }

    @Test
    public void testBookTimeSlot_Success() {
        TimeSlot timeSlot = new TimeSlot();
        timeSlot.setId(1L);
        timeSlot.setBooked(false);
        TimeSlotDTO timeSlotDTO = new TimeSlotDTO(1L, DeliveryMethod.DRIVE, LocalDate.now(), LocalTime.now(), LocalTime.now(), true);
        when(timeSlotRepository.findById(1L)).thenReturn(Mono.just(timeSlot));
        when(timeSlotRepository.save(any(TimeSlot.class))).thenReturn(Mono.just(timeSlot));
        when(timeSlotMapper.toTimeSlotDTO(any(TimeSlot.class))).thenReturn(timeSlotDTO);

        Mono<TimeSlotDTO> result = deliveryService.bookTimeSlot(1L);

        StepVerifier.create(result)
                .expectNext(timeSlotDTO)
                .verifyComplete();
    }

    @Test
    public void testBookTimeSlot_WhenSlotIsAlreadyBooked() {
        TimeSlot timeSlot = new TimeSlot();
        timeSlot.setBooked(true);
        when(timeSlotRepository.findById(1L)).thenReturn(Mono.just(timeSlot));

        Mono<TimeSlotDTO> result = deliveryService.bookTimeSlot(1L);

        verify(timeSlotRepository, times(1)).findById(1L);
        verify(timeSlotRepository, never()).save(any(TimeSlot.class));
        verify(timeSlotMapper, never()).toTimeSlotDTO(any(TimeSlot.class));
        result
                .doOnError(error -> {
                    assert(error instanceof TimeSlotAlreadyBookedException);
                })
                .subscribe();
    }
}
