package com.example.driveanddeliver.controller;

import com.example.driveanddeliver.controller.dtos.TimeSlotDTO;
import com.example.driveanddeliver.domain.DeliveryMethod;
import com.example.driveanddeliver.service.DeliveryService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Base64;
import java.util.List;

import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureWebTestClient
public class DeliveryControllerTest {

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private DeliveryService deliveryService;

    @Test
    public void testGetAvailableTimeSlots() {
        TimeSlotDTO timeSlot1 = new TimeSlotDTO(1L, DeliveryMethod.DRIVE, LocalDate.now(), LocalTime.of(9, 0), LocalTime.of(10, 0), false);
        TimeSlotDTO timeSlot2 = new TimeSlotDTO(2L, DeliveryMethod.DELIVERY, LocalDate.now(), LocalTime.of(10, 0), LocalTime.of(11, 0), false);
        when(deliveryService.findAvailableTimeSlots(DeliveryMethod.DRIVE, LocalDate.now()))
                .thenReturn(Flux.just(timeSlot1, timeSlot2));
        String authHeader = "Basic " + Base64.getEncoder().encodeToString("user:password".getBytes(StandardCharsets.UTF_8));

        webTestClient.get()
                .uri("/api/delivery/available-slots?method=DRIVE&date=2024-05-27")
                .header(HttpHeaders.AUTHORIZATION, authHeader)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBodyList(TimeSlotDTO.class)
                .isEqualTo(List.of(timeSlot1, timeSlot2));
    }
    @Test
    public void testBookTimeSlotUnauthorized() {
        TimeSlotDTO timeSlot1 = new TimeSlotDTO(1L, DeliveryMethod.DRIVE, LocalDate.now(), LocalTime.of(9, 0), LocalTime.of(10, 0), false);
        TimeSlotDTO timeSlot2 = new TimeSlotDTO(2L, DeliveryMethod.DELIVERY, LocalDate.now(), LocalTime.of(10, 0), LocalTime.of(11, 0), false);
        when(deliveryService.findAvailableTimeSlots(DeliveryMethod.DRIVE, LocalDate.now()))
                .thenReturn(Flux.just(timeSlot1, timeSlot2));

        webTestClient.get()
                .uri("/api/delivery/available-slots?method=DRIVE&date=2024-05-27")
                .exchange()
                .expectStatus().isUnauthorized();
    }
    @Test
    public void testBookTimeSlot() {
        TimeSlotDTO timeSlot = new TimeSlotDTO(1L, DeliveryMethod.DRIVE, LocalDate.now(), LocalTime.of(9, 0), LocalTime.of(10, 0), false);
        when(deliveryService.bookTimeSlot(1L)).thenReturn(Mono.just(timeSlot));
        String authHeader = "Basic " + Base64.getEncoder().encodeToString("user:password".getBytes(StandardCharsets.UTF_8));
        webTestClient.post().uri("/api/delivery/book-slot/1")
                .header(HttpHeaders.AUTHORIZATION, authHeader)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(TimeSlotDTO.class)
                .isEqualTo(timeSlot);
    }
    @Test
    public void testBookTimeSlotAnauthorized() {
        TimeSlotDTO timeSlot = new TimeSlotDTO(1L, DeliveryMethod.DRIVE, LocalDate.now(), LocalTime.of(9, 0), LocalTime.of(10, 0), false);
        when(deliveryService.bookTimeSlot(1L)).thenReturn(Mono.just(timeSlot));
        webTestClient.post().uri("/api/delivery/book-slot/1")
                .exchange()
                .expectStatus().isUnauthorized();
    }
}

