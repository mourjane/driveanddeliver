# DriveAndDeliver

DriveAndDeliver is a Spring Boot application for managing delivery methods and time slots for customers. This project includes setting up the environment, running the application locally, deploying it using Docker and Kubernetes, and using GitLab CI/CD for continuous integration and delivery.

## Table of Contents

- [Requirements](#requirements)
- [Getting Started](#getting-started)
    - [Clone the Repository](#clone-the-repository)
    - [Build the Project](#build-the-project)
    - [Run the Tests](#run-the-tests)
    - [Run the Application](#run-the-application)
- [Docker](#docker)
    - [Build Docker Image](#build-docker-image)
    - [Run Docker Container](#run-docker-container)
- [Kubernetes](#kubernetes)
    - [Deploy to Kubernetes](#deploy-to-kubernetes)
- [Delivery Service API](#endpoints-documentation)
    - [Available Time Slots](#available-time-slots)
    - [Book Time Slot](#book-time-slot)
## Requirements

- Java 21
- Maven
- Docker
- Kubernetes CLI (`kubectl`)
- GitLab CI/CD

## Getting Started

### Clone the Repository

```bash
git clone https://gitlab.com/mourjane/driveanddeliver.git
cd driveanddeliver
```
### Build the Project

```bash
mvn clean compile
```
### Run the Tests

```bash
mvn test
```
### Run the Application

```bash
mvn spring-boot:run
```
The application will be available at http://localhost:8080.

## Docker

### Build Docker Image

```bash
docker build -t driveanddeliver .
```

### Run Docker Container

```bash
docker run -p 8080:8080 driveanddeliver
```
## Kubernetes

### Deploy to Kubernetes
Ensure you have a Kubernetes cluster running and kubectl configured. Then apply the deployment file:
```bash
kubectl apply -f k8s/deployment.yaml
```

## Endpoints Documentation

# Delivery Service API

## Available Time Slots

### URL: `/api/delivery/available-slots`
- **Method:** GET
- **Parameters:**
    - `method`: The delivery method (DRIVE, DELIVERY, DELIVERY_TODAY, DELIVERY_ASAP)
    - `date`: The delivery date (YYYY-MM-DD)
- **Response:** List of available time slots.

### Example Request
```bash
curl --location 'http://localhost:8080/api/delivery/available-slots?method=DRIVE&date=2024-06-01' \
--header 'Authorization: Basic dXNlcjpwYXNzd29yZA=='
```

## Book Time Slot

### URL: `/api/delivery/book-slot/{id}`
- **Method:** POST
- **Parameters:**
    - `id`: Path variable (Time Slot ID)
- **Response:** Details of the booked time slot or an error if the slot is already booked.

### Example Request
```bash
curl --location --request POST 'http://localhost:8080/api/delivery/book-slot/1' \
--header 'Authorization: Basic dXNlcjpwYXNzd29yZA=='
```

