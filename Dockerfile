FROM openjdk:21-jdk-slim

# Set the working directory
WORKDIR /

# Copy the packaged jar file into the container
COPY target/driveanddeliver-*.jar app.jar

# Make port 8080 available to the world outside this container
EXPOSE 8080

# Run the jar file
ENTRYPOINT ["java", "-jar", "app.jar"]
